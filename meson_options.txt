option(
    'backend-ilm',
    type: 'boolean',
    value: false,
    description: 'Enable the ILM QAD backend'
)

option(
    'static',
    type: 'boolean',
    value: true,
    description: 'Link statically where possible'
)
