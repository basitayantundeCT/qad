/*
 * Copyright © 2022 Codethink Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <backend.h>

#define BTN_DOWN 1
#define BTN_UP 0

qad_backend_input_t *create_input_backend(char input_type[]);

// int move(int x, int y, int event);

// int button(int value, int event);

// int touch(int x, int y, int duration, int event);

// int swipe(int x, int y, int x_2, int y_2, int velocity, int event);

int send_syn_event(int event);

int send_button_event(int value, int event);

int send_pressure_event(int value, int event);

int send_major_event(int value, int event);

int send_position_event_abs(int x, int y, int event);

int send_position_event_mt(int x, int y, int event);

int send_tracking_event(int value, int event);

int send_swipe_header(int major_value, int pressure, int event);

int send_swipe_footer(int event);

int send_touch(int x, int y, int duration, int event);

int send_swipe(int x, int y, int x_2, int y_2, int v, int event);

int send_key_event(int key, int fd);

int send_text_event(int const *key_codes, int num_keycodes, int fd);
