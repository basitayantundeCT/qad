/*
 * Copyright © 2022 Codethink Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __QAD_BACKENDS_H__
#define __QAD_BACKENDS_H__

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define QAD_CHECK_ERR(expression)                                              \
  do {                                                                         \
    int expression_error = (expression);                                       \
    if (expression_error < 0) {                                                \
      char const *err_msg = strerror(errno);                                   \
      printf("error: %d %s, line: %d, file: %s\n", expression_error, err_msg,  \
             __LINE__, __FILE__);                                              \
      fflush(stdout);                                                          \
      abort();                                                                 \
    }                                                                          \
  } while (0)

#define BUFFER_TYPE_PNG 0
#define BUFFER_TYPE_BMP 1

typedef struct qad_screen_buffer_s {
  unsigned char *buffer;
  int buffer_size;
  int type;
} qad_screen_buffer_t;

typedef struct qad_backend_input_s {
  int (*move)(int, int, int);
  int (*button)(int, int);
  int (*touch)(int, int, int, int);
  int (*swipe)(int, int, int, int, int, int);
  int (*key)(int, int);
  int (*text)(int const *, int, int);
  void *data;
} qad_backend_input_t;

typedef struct qad_backend_screen_s {
  qad_screen_buffer_t *(*grab_fb)(int screen);
  void (*list_fbs)(char *reply);
} qad_backend_screen_t;

typedef struct qad_backend_s {
  qad_backend_screen_t *screen_backend;
  qad_backend_input_t *input_backend;
} qad_backend_t;

#endif
